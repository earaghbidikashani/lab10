#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary)
{
    // Open the dictionary file
    FILE *fdic;
    fdic = fopen(dictionary, "r");
    if(!fdic){
        printf("Invalid Input!\n");
        exit(1);
    }
    
    // Loop through the dictionary file, one line
    // at a time.
    char line[255];
    while(fgets(line, 255, fdic) != NULL){
        
        //remove the new line character
        char *c = line;
        int len = 0;
        while(c != '\0'){
            if(*c == '\n'){
                *c = '\0';
                break;
            }
            c++;
            len++;
        }
        // Hash each password. Compare to the target hash.
        // If they match, return the corresponding password.
        char *wr;
        wr = md5(line, len);
        if(strcmp(wr, target) == 0){
            free(fdic);
            return line;
        }
    }
    // Free up memory?
    fclose(fdic);
    return NULL;
}


int main(int argc, char *argv[])
{
    
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    char *file;
    file = argv[2];
    // Open the hash file for reading.
    FILE *fp;
    fp = fopen(argv[1], "r");
    if(!fp){
        printf("Invalid Input!\n");
        exit(1);
    }

    // For each hash, crack it by passing it to crackHash
    char line[255];

    while(fgets(line, 255, fp)!= '0'){
        
        char *c = line;
        while(c != '\0'){
            if(*c == '\n'){
                *c = '\0';
                break;
            }
            c++;
        }
        char *s = crackHash(line, file);
        // Display the hash along with the cracked password:
        printf("Hash: %s\n", line);
        printf("Password: %s\n", s);
    }
    
    
    // Close the hash file
    fclose(fp);
    return 0;
}
